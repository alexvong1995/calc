/* toy calculator made using flex (through libguile) and lalr-scm
   Copyright (C) 2016  Alex Vong

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef CALC_H
#define CALC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libguile.h>

#ifndef __GNUC__
#define  __attribute__(x) /* nothing  */
#endif /* __GNUC__  */

/* flex internally uses yyscanner as variable name, we must stick to it  */
#define YY_DECL SCM yylex(yyscanner) yyscan_t yyscanner;

/* avoid implicit declarations  */
extern int yylex_init(yyscan_t *scanner);
extern SCM yylex(yyscan_t yyscanner);
extern int yylex_destroy(yyscan_t yyscanner);

/* unused functions  */
static void yyunput(int c, char * yy_bp, yyscan_t yyscanner)
  __attribute__((unused));
static int input(yyscan_t yyscanner)
  __attribute__((unused));

/* struct of typedef'd pointers  */
typedef struct scanner_buf_state_pair_struct
{
  yyscan_t scanner;
  YY_BUFFER_STATE buf_state;
} scanner_buf_state_pair;

/* union used for type punning  */
typedef union fp_voidp_union
{
  SCM (*fp_0)(void);
  SCM (*fp_1)(SCM);
  void *voidp;
} fp_voidp_pair;

SCM ZERO; /* immutable  */
SCM HASH_TAB; /* mutable  */
SCM MAKE_LEXICAL_TOK; /* immutable  */
fp_voidp_pair TOK_ITERATOR; /* immutable  */
fp_voidp_pair MAKE_TOK_ITERATOR; /* immutable  */

static inline SCM
make_lexical_tok(SCM category, SCM src, SCM val)
{
  return scm_call(MAKE_LEXICAL_TOK, category, src, val, SCM_UNDEFINED);
}

static inline SCM
num_to_tok(char *category_str, char *num_str)
{
  return \
    make_lexical_tok(scm_from_locale_symbol(category_str),
                     SCM_BOOL_F,
                     scm_c_locale_stringn_to_number(num_str,
                                                    strlen(num_str),
                                                    10));
}

static inline SCM
sym_to_tok(char *category_str, char *sym_str)
{
  return make_lexical_tok(scm_from_locale_symbol(category_str),
                          SCM_BOOL_F,
                          scm_from_locale_symbol(sym_str));
}

static inline void
destroy_scanner_buf_state_pair(void *ptr)
{ scanner_buf_state_pair* pair = ptr;

  yy_delete_buffer(pair->buf_state, pair->scanner);
  yylex_destroy(pair->scanner);
  free(pair);
}

/* 1. get the procedure on the top of the stack
   2. use the procedure to get a scheme value from the hash table
   3. validate the value is a wrapped scanner_buf_state_pair
   4. return the token returned by the scanner  */
static inline SCM
tok_iterator(void)
{ SCM proc = scm_frame_procedure(scm_stack_ref(scm_make_stack(SCM_BOOL_T,
                                                              SCM_EOL),
                                               ZERO));
  SCM val = scm_hashq_ref(HASH_TAB, proc, SCM_BOOL_F);
  scanner_buf_state_pair* pair;
  yyscan_t scanner;

  if (scm_is_false(val))
    scm_error(scm_from_locale_symbol("misc-error"),
              NULL,
              "Procedure not in hash table: ~A",
              scm_list_1(proc),
              SCM_BOOL_F);

  pair = scm_to_pointer(val);
  scanner = pair->scanner;
  return yylex(scanner);
}

/* 1. generate a unique identifier
   2. create a procedure using the identifier
   3. create and initialize a scanner_buf_state_pair
   4. put the procedure and the wrapped scanner_buf_state_pair
   into the hash table
   using the procedure as key and the scanner_buf_state_pair as value
   5. perform cleanup
   6. return the procedure  */
static inline SCM
make_tok_iterator(SCM val)
{ char *input_str = scm_to_locale_string(val);
  SCM proc_name_sym = scm_gensym(scm_from_locale_string("token-iterator-"));
  char *proc_name_str = \
    scm_to_locale_string(scm_symbol_to_string(proc_name_sym));
  SCM proc = scm_c_make_gsubr(proc_name_str,
                              0, 0, 0,
                              TOK_ITERATOR.voidp);
  scanner_buf_state_pair *pair = \
    scm_malloc(sizeof(scanner_buf_state_pair));

  yylex_init(&(pair->scanner));
  pair->buf_state = yy_scan_string(input_str, pair->scanner);
  scm_hashq_set_x(HASH_TAB,
                  proc,
                  scm_from_pointer(pair,
                                   &destroy_scanner_buf_state_pair));

  free(input_str);
  free(proc_name_str);
  return proc;
}

/* initialize globals and register function to be used in scheme  */
void
init_calc(void)
{
  ZERO = scm_from_int(0);
  HASH_TAB = scm_make_weak_key_hash_table(ZERO);
  MAKE_LEXICAL_TOK = scm_call(scm_c_public_ref("guile",
                                               "record-constructor"),
                              scm_c_private_ref("system base lalr",
                                                "lexical-token"),
                              SCM_UNDEFINED);
  TOK_ITERATOR.fp_0 = &tok_iterator;
  MAKE_TOK_ITERATOR.fp_1 = &make_tok_iterator;

  scm_c_define_gsubr("make-token-iterator",
                     1, 0, 0,
                     MAKE_TOK_ITERATOR.voidp);
}

#endif /* CALC_H  */
