#!/bin/sh
# -*- Scheme -*-
if test -x "$HOME/.guix-profile/bin/guile"
then
GUILE="$HOME/.guix-profile/bin/guile"
elif test -x "/usr/local/bin/guile"
then
GUILE="/usr/local/bin/guile"
else
GUILE="/usr/bin/guile"
fi
exec "$GUILE" -e main -s "$0" "$@"
!#
;;; toy calculator made using flex (through libguile) and lalr-scm
;;; Copyright (C) 2016  Alex Vong

;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(load-extension "./libguile-calc" "init_calc")
(use-modules (system base lalr)
             (ice-9 rdelim)
             (rnrs io ports))

(define parser
  (let ((eof-char (eof-object)))
    (lalr-parser
     (num + - * /)
     (program (exp): $1
              (*eoi*): eof-char)
     (exp (num): $1
          (exp exp +): (+ $1 $2)
          (exp exp -): (- $1 $2)
          (exp exp *): (* $1 $2)
          (exp exp /): (/ $1 $2)))))

(define main
  (lambda _
    (display (parser (make-token-iterator (read-line))
                     error))
    (newline)))
