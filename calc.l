/* toy calculator made using flex (through libguile) and lalr-scm
   Copyright (C) 2016  Alex Vong

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

%{ /* -*- bison-mode -*- since I can't find any flex-mode...  */
#include "calc.h"
%}

/* NOYYWRAP will never be called since <<EOF>> is explicitely handled below  */
%option noyywrap

/* reentrancy is a must since user may create mutiple tokenizers  */
%option reentrant

DIGIT [0-9]

%%

[ \t\n]+ /* skip over them  */

{DIGIT}*"."*{DIGIT}+ {return num_to_tok("num", yytext);}

"+"|"-"|"*"|"/" {return sym_to_tok(yytext, yytext);}

<<EOF>> {return sym_to_tok("*eoi*", "*eoi*");}

. /* signal an error  */

%%
