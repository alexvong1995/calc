#!/bin/sh
# -*- Scheme -*-
if test -x "$HOME/.guix-profile/bin/guile"
then
GUILE="$HOME/.guix-profile/bin/guile"
elif test -x "/usr/local/bin/guile"
then
GUILE="/usr/local/bin/guile"
else
GUILE="/usr/bin/guile"
fi
exec "$GUILE" -e main -s "$0" "$@"
!#
;;; toy calculator made using flex (through libguile) and lalr-scm
;;; Copyright (C) 2016  Alex Vong

;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-load-path ".")
(use-modules (make make))

(define (all)
  (let ((lexflags "-Cfe")
        (cflags "`pkg-config --cflags guile-2.0` -shared -fPIC -O2 -Wall -Wextra -pedantic"))
    (make/proc
     `(("libguile-calc.so" ("calc.yy.c")
        ,(lambda ()
           (system
            (string-join
             `("gcc -o libguile-calc.so" ,cflags "calc.yy.c")))))
       ("calc.yy.c" ("calc.l" "calc.h")
        ,(lambda ()
           (system
            (string-join
             `("flex -o calc.yy.c" ,lexflags "calc.l")))))))))

(define (clean)
  (make/proc
   `(("" ()
      ,(lambda ()
         (system "rm -f libguile-calc.so calc.yy.c"))))))

(define (main arg-ls)
  (let ((args (cdr arg-ls)))
    (cond ((= (length args) 0)
           (all))
          ((and (= (length args) 1)
                (string=? (car args) "all"))
           (all))
          ((and (= (length args) 1)
                (string=? (car args) "clean"))
           (clean))
          (else
           (format #t
                   "make: *** Don't know how to make target `~a'.  Stop.~%"
                   (car args))))))
