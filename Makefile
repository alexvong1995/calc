# toy calculator made using flex (through libguile) and lalr-scm
# Copyright (C) 2016  Alex Vong

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

LEXFLAGS = -Cfe
CFLAGS = `pkg-config --cflags guile-2.0` -shared -fPIC -O2 -Wall -Wextra -pedantic

.PHONY: all pp clean

all: libguile-calc.so

libguile-calc.so: calc.yy.c
	gcc -o libguile-calc.so $(CFLAGS) calc.yy.c

pp:
	cpp $(CFLAGS) calc.yy.c | less

calc.yy.c: calc.l calc.h
	flex -o calc.yy.c $(LEXFLAGS) calc.l

clean:
	rm -f libguile-calc.so calc.yy.c
